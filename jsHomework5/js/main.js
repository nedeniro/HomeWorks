"use strict"

let person = {
    name: "Denis",
    age: 26,
    info: {
    gender: "male",
    job: "developer"
}
}

function cloneObj(obj) {
    let objClone = {};
    for (let key in obj) {
        if (obj[key] == true && Array.isArray(obj)) {
            return objClone[key] = cloneObj(obj[key]);
        } else {
            objClone[key] = obj[key];
        }
    }
    return objClone;
}

let obj2 = cloneObj(person);
console.log(obj2);

