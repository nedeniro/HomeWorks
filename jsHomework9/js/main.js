userDateBirth = prompt("Enter your date birth", "DD.MM.YYYY").split(".", 3);
let userBirth = new Date(userDateBirth[2], userDateBirth[1] - 1, userDateBirth[0]);

function dateBirth(userBirth) {
    let dateToday = new Date();
    let age = dateToday.getFullYear() - userBirth.getFullYear();
    let month = dateToday.getMonth() - userBirth.getMonth();
    if (month < 0 || month == 0 && dateToday.getDate() < userBirth.getDate()) {
        age--;
    }
    return "Ваш возраст " + age + " лет!";
}

let signsZodiacs = [
    "Козерог",
    "Водолей",
    "Рыбы",
    "Овен",
    "Телец",
    "Близнецы",
    "Рак",
    "Лев",
    "Дева",
    "Весы",
    "Скорпион",
    "Стрелец",
];

function GetSignZodiac() {
    let zodiacMonth = userBirth.getMonth()+1;
    let zodiacDay = userBirth.getDate();
    if (zodiacMonth == 12 && zodiacDay > 21 || zodiacMonth == 01 && zodiacDay <= 19) {
        return "Ваш знак зодиака " + signsZodiacs[0] + "!";
    } else if (zodiacMonth == 01 && zodiacDay > 21 || zodiacMonth == 02 && zodiacDay <= 18) {
        return "Ваш знак зодиака " + signsZodiacs[1] + "!";
    } else if (zodiacMonth == 02 && zodiacDay > 18 || zodiacMonth == 03 && zodiacDay <= 20) {
        return "Ваш знак зодиака " + signsZodiacs[2] + "!";
    } else if (zodiacMonth == 03 && zodiacDay > 20 || zodiacMonth == 04 && zodiacDay <= 20) {
        return "Ваш знак зодиака " + signsZodiacs[3] + "!";
    } else if (zodiacMonth == 04 && zodiacDay > 20 || zodiacMonth == 05 && zodiacDay <= 20) {
        return "Ваш знак зодиака " + signsZodiacs[4] + "!";
    } else if (zodiacMonth == 05 && zodiacDay > 20 || zodiacMonth == 06 && zodiacDay <= 21) {
        return "Ваш знак зодиака " + signsZodiacs[5] + "!";
    } else if (zodiacMonth == 06 && zodiacDay > 21 || zodiacMonth == 07 && zodiacDay <= 22) {
        return "Ваш знак зодиака " + signsZodiacs[6] + "!";
    } else if (zodiacMonth == 07 && zodiacDay > 22 || zodiacMonth == 08 && zodiacDay <= 23) {
        return "Ваш знак зодиака " + signsZodiacs[7] + "!";
    } else if (zodiacMonth == 08 && zodiacDay > 23 || zodiacMonth == 09 && zodiacDay <= 23) {
        return "Ваш знак зодиака " + signsZodiacs[8] + "!";
    } else if (zodiacMonth == 09 && zodiacDay > 23 || zodiacMonth == 10 && zodiacDay <= 23) {
        return "Ваш знак зодиака " + signsZodiacs[9] + "!";
    } else if (zodiacMonth == 10 && zodiacDay > 23 || zodiacMonth == 11 && zodiacDay <= 22) {
        return "Ваш знак зодиака " + signsZodiacs[10] + "!";
    } else {
        return "Ваш знак зодиака " + signsZodiacs[11] + "!";
    }
}

alert(dateBirth(userBirth));
alert(GetSignZodiac());
