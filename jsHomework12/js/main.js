function showFields() {
    firstBtn.remove();

    let valueDiv = document.createElement('div');
    valueDiv.id = 'valueDiv';
    valueDiv.innerHTML = '<div id="circle"></div><div class="fieldsDiv"><input id="sizeField" style="text" placeholder="Enter size" ><input id="colorField" style="text" placeholder="Enter color"></div><div class="btnDiv"><button id="secondBtn" type="submit" onclick="someFunc()">Нарисовать</button></div>';
    document.body.appendChild(valueDiv);
}
firstBtn.addEventListener("click", showFields);

function someFunc() {
    let visibility = "block";
    document.getElementById('circle').style.display = visibility;

    userCircleSize = document.getElementById('sizeField').value;
    finishCircleSize = (userCircleSize * 2) + 'px';
    document.getElementById('circle').style.height = finishCircleSize;
    document.getElementById('circle').style.width = finishCircleSize;

    userCircleColor = document.getElementById('colorField').value;
    document.getElementById('circle').style.background = userCircleColor;
}
