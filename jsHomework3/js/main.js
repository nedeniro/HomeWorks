let n = prompt("Enter value", "");
function calcFact(n) {
    if (n == 1 || n == 0) {
        return 1;
    }
    else if (n < 0) {
        return "Wrong value!";
    }
    else if (n == null) {
        return "Enter correct value!";
    }
    else if (n > 1 || n == Infinity) {
        return n * calcFact(n-1);
    }
    else {
        return "It's not a number!";
    }
}
alert(calcFact(n));