function createNewUser() {

    Object.defineProperty(this, "firstName", {
        value: prompt("Enter your name", ""),
        writable: false,
        configurable: true
    })

    this.setFirstName = function (newName) {
        Object.defineProperty(this, "firstName", {
            value: newName
        })
    }

    Object.defineProperty(this, "lastName", {
        value: prompt("Enter your lastname", ""),
        writable: false,
        configurable: true
    })

    this.setLastName = function (newLastName) {
        Object.defineProperty(this, "lastname", {
            value: newLastName
        })
    }

}

let person = new createNewUser();

let getLogin = person.firstName.substring(0, 1).toLowerCase() + person.lastName.toLowerCase();
